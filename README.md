nrvale0/foo
-----------

Puppet class used to demonstrate data-in-modules when implemented
via the ripienaar/module_data Puppet Forge module.

# Usage
    $ puppet module install ripienaar/module_data
    $ puppet apply --modulepath=<MODULE PATH>:. foo/tests/init.pp
    $ cd foo & hiera -c data/hiera_test.yaml foo::somedata
    from common.yaml in foo/data

