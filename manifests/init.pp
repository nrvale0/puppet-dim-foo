class foo(
  $somedata = "from foo's default class parameter value"
) {
  notify { "The value of \$somedata is sourced $somedata.": }
}
